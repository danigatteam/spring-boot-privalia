package com.privalia.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.stereotype.Component;

import com.privalia.services.BugService;

@Component
public class BugsEndpoint extends AbstractEndpoint<List<String>> {

    @Autowired
    BugService bugService;

    public BugsEndpoint() {
	super("bugs");
    }

    @Override
    public List<String> invoke() {
	return bugService.listAllBugs();
    }

}
