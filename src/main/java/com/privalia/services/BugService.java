package com.privalia.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;


@Service
public class BugService {

	public List<String> listAllBugs() {
		
		List<String> bugs = new ArrayList<>();
		
		bugs.add("Bug 1");
		bugs.add("Otro bug");
		
		return bugs;
	}
}
